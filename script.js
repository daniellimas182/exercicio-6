var filmesJogos = [];

function init() {
    document.getElementById('enviar').addEventListener('click', adicionarItem);
}

function getProxId(){
    if(filmesJogos.length == 0){
        return 1;
    }

    return filmesJogos[filmesJogos.length-1].id +1;
}

function adicionarItem() {
    filmesJogos.push({
        id: getProxId(),
        nome: document.getElementById('nome').value,
        estilo: document.getElementById('estilo').value,
        urlFoto: document.getElementById('urlFoto').value
    });

    var div = document.getElementById('content');
    div.innerHTML = null;

    for (var fj of filmesJogos) {
        var nome = document.createElement('p');
        var estilo = document.createElement('p');
        var imagem = document.createElement('img');

        nome.innerHTML = fj.nome;
        div.appendChild(nome);

        estilo.innerHTML = fj.estilo;
        div.appendChild(estilo);

        imagem.src = fj.urlFoto;
        div.appendChild(imagem);
    }

}

init();
